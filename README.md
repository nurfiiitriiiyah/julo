
# Mini Wallet Exercise

This is API for mini wallet Exercise, the requirement is python v3 
'

## Authors

- [Nur Fitriyah](https://www.linkedin.com/in/nur-fitriyah-0b7163141/)


## Tech Stack

**Database:** Mysql, Redis

**Language:** Python


## Documentation

[Documentation](https://docs.google.com/document/d/e/2PACX-1vSsTx5M0T0PTZEsIudmJoTFRLwqseJYthpmS9SkyhF1iBLaOXMYaRVKBHkdqTKkKuNxCmP8nKx7gdOM/pub)


## How to run

First install all dependencies
```
pip3 install -r /path/to/requirements.txt  
```
Run docker compose to install redis and mysql
```
docker-compose up
```
After docker compose run succesfully, please import run the sql script. Run the app using
```
python3 -m uvicorn main:app --reload
```

