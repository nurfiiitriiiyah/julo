-- julo.tb_customer definition

CREATE TABLE `tb_customer` (
  `customer_xid` varchar(100) NOT NULL,
  `created_at` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


-- julo.tb_token definition

CREATE TABLE `tb_token` (
  `token` varchar(100) DEFAULT NULL,
  `customer_xid` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


-- julo.tb_transaction definition

CREATE TABLE `tb_transaction` (
  `transaction_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `reference_id` varchar(255) DEFAULT NULL,
  `customer_xid` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `transaction_type` varchar(100) DEFAULT NULL,
  `amout` float DEFAULT NULL,
  PRIMARY KEY (`transaction_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


-- julo.tb_wallet definition

CREATE TABLE `tb_wallet` (
  `wallet_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `customer_xid` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `enabled_at` datetime DEFAULT NULL,
  `balance` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;