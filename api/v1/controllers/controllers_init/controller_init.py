from ...services.service_init import service_init
from ...models.model_request import Init,WalletDepositsWithdrawals
from ...models.model_response import ModelInitRes

def Init(data: Init)->ModelInitRes:
    return service_init.ServiceInit(data)

def EnableWallet(token:str, id: str):
    res=service_init.ServiceEnableWallet(token,id)
    return res
def GetWallet(id):
    res=service_init.ServiceGetWallet(id)
    return res
def ViewTransaction(id:str):
    res=service_init.GetListTrasaction(id)
    return res
def DepositsWallet(data:WalletDepositsWithdrawals,id:str):
    res = service_init.DepositWallet(data,id)
    return res
def WithdrawalsWallet(data:WalletDepositsWithdrawals,id:str):
    res = service_init.WithdrawalsWallet(data,id)
    return res
def DisabledWallet(id:str):
    res = service_init.DisableWallet(id)
    return res