from ...models.model_request import Init,WalletDepositsWithdrawals
from ...models.model_response import ModelInitRes,EnableWallet,detailWallet,TransactionList
from ...db.db_config import Insert,Update, FindOne, FindAll
from ...db.redis import SetRedis
import uuid

myuuid = uuid.uuid4()
from hashlib import md5
 

def Md5(id)->str:
  result:str = md5(id.encode())
  return result.hexdigest()

def ServiceInit(data=Init)->ModelInitRes: 
  result = ModelInitRes()
  result.token=Md5(data.customer_xid)
  SetRedis(result.token,data.customer_xid,5*60)
  return result

def ServiceEnableWallet(token:str,id:str):
  Update('tb_wallet','status="enabled"','customer_xid="{}"'.format(id))
  result=FindOneService(id)
  results=EnableWallet()
  results.wallet=result
  return results

def FindOneService(id):
  get_one= FindOne('customer_xid="{}"'.format(id), 'tb_wallet')
  result=detailWallet()
  result.id=get_one[0]
  result.owned_by=get_one[1]
  result.status=get_one[2]
  result.enabled_at=get_one[3]
  result.balance=get_one[4]

  return result
def ServiceGetWallet(id):
  result=FindOneService(id)
  results=EnableWallet()
  results.wallet=result
  return results

def DepositWallet(data:WalletDepositsWithdrawals,id:str):
  get_latest_balance = FindOneService(id)
  balance=get_latest_balance.balance+float(data.amount)
  Insert("transaction_id,reference_id,customer_xid,transaction_type,amout",'"{}","{}","{}","deposit",{}'.format( str(uuid.uuid4()),data.reference_id,id,data.amount),"tb_transaction" )
  Update('tb_wallet','balance={}'.format(balance),'customer_xid="{}"'.format(id))
  return FindOneService(id)

def WithdrawalsWallet(data:WalletDepositsWithdrawals,id:str):
  get_latest_balance = FindOneService(id)
  balance=get_latest_balance.balance-float(data.amount)
  Insert("transaction_id,reference_id,customer_xid,transaction_type,amout",'"{}","{}","{}","deposit",{}'.format( str(uuid.uuid4()),data.reference_id,id,data.amount),"tb_transaction" )
  Update('tb_wallet','balance={}'.format(balance),'customer_xid="{}"'.format(id))
  return FindOneService(id)

def DisableWallet(id):
  Update('tb_wallet','status="disabled"','customer_xid="{}"'.format(id))
  result=FindOneService(id)
  results=EnableWallet()
  results.wallet=result
  return results

def GetListTrasaction(id):
  reesults=[]
  
  get_all= FindAll('customer_xid="{}"'.format(id), 'tb_transaction','transaction_id,reference_id,transaction_type,amout')
  for v in get_all:
      result=TransactionList()
      result.transaction_id= v[0]
      result.customer_xid=v[1]
      result.type=v[2]
      result.amout=v[3]
  
      reesults.append(result)
    
  return reesults