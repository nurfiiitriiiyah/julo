from pydantic import BaseModel, validator
from typing import Annotated
from fastapi import  Form

class Init:    
    customer_xid:str


class WalletDepositsWithdrawals:
    amount: float
    reference_id: str

class WalletDisables():
    is_disabled: bool
