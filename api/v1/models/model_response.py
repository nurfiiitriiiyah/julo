class ModelInitRes:
    token: str
class detailWallet:
    id: str
    owned_by: str
    status: str 
    enabled_at: str
    balance: float
class EnableWallet:
    wallet: type[detailWallet]
class TransactionList:
    transaction_id: str
    customer_xid: str
    amout : float
    reference_id: str
    type: str