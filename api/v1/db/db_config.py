import mysql.connector

mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  password="password",
  database="julo",
  
)
mycursor = mydb.cursor()
def Insert(coloumn,data,table): 
    sql:str="INSERT INTO {} ({}) VALUES({}) ".format(table,coloumn,data)
    mycursor.execute(sql)
    mydb.commit()
    return
def Update(table, data, where):
    sql:str = "UPDATE {} SET {} WHERE {}".format(table,data,where) 
    mycursor.execute(sql)
    mydb.commit()
    return 
def FindAll(where,table, select):
    sql:str = "SELECT {} FROM {} WHERE {}     ".format(select,table,where)
    mycursor.execute(sql)
    result =mycursor.fetchall()
    return result
def FindOne(where,table):
    sql:str = "SELECT * FROM {} WHERE {} LIMIT 1    ".format(table,where)
    mycursor.execute(sql)
    result =mycursor.fetchone()
    return result
