import redis
r = redis.Redis(host='localhost', port=6379, decode_responses=True)

def SetRedis(key:str, value, TTL):
    return r.set(key, value)
    
def GetRedis(key):
    return r.get(key)