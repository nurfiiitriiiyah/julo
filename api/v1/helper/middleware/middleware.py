from ...db.redis import GetRedis
from fastapi import HTTPException 
def CheckToken(token):
    check = GetRedis(token)
    if(check):
        return check
    else:
        raise HTTPException(401, detail='Token invalid')