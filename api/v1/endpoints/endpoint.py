import  os
ROOT_DIR = (os.path.dirname(os.path.abspath(__file__))).replace("api/v1/endpoints","")

from fastapi import APIRouter,Form,Header, Depends,HTTPException
from fastapi_versioning import  version
from typing import Annotated, Union

from ..models.model_request import Init,WalletDepositsWithdrawals
from ..controllers.controllers_init import controller_init
from ..helper.mapping.mapping_response import mapping_success
from ..helper.middleware.middleware import CheckToken
router = APIRouter()
sessions = {}


@version(1)

async def verify_token(Authorization: Annotated[str, Header()]):
    if (Authorization):
        get_token = CheckToken(Authorization[6:])
        sessions["id"] = get_token
        
        return Authorization
    else:
        raise HTTPException(status_code=400, detail="Auth invalid")
    
    
@router.post("/init")
async def initToken(customer_xid: Annotated[str, Form()]):
     data=Init()
     data.customer_xid=customer_xid
     return mapping_success(controller_init.Init(data))

@router.post("/wallet",dependencies=[Depends(verify_token)])
async def EnableWallet(
    Authorization: Annotated[Union[str, None], 
                             Header()] = None):
    return mapping_success(controller_init.EnableWallet(Authorization,sessions["id"]))

@router.get("/wallet",dependencies=[Depends(verify_token)])
async def ViewWallet(Authorization: Annotated[Union[str, None], Header()] = None):
   return mapping_success(controller_init.GetWallet(sessions["id"]))

@router.get("/wallet/transactions",dependencies=[Depends(verify_token)])
async def ViewTransaction(Authorization: Annotated[Union[str, None], Header()] = None):
    return mapping_success(controller_init.ViewTransaction(sessions["id"]))

@router.post("/wallet/deposits",dependencies=[Depends(verify_token)])
def Deposits(amount: Annotated[str, Form()],reference_id: Annotated[str, Form()]):
    data= WalletDepositsWithdrawals()
    data.amount=amount
    data.reference_id=reference_id
    return mapping_success(controller_init.DepositsWallet(data,sessions["id"]))

@router.post("/wallet/withdrawals",dependencies=[Depends(verify_token)])
async def Withdrawals(amount: Annotated[str, Form()],reference_id: Annotated[str, Form()]):
    data= WalletDepositsWithdrawals()
    data.amount=amount
    data.reference_id=reference_id
    return mapping_success(controller_init.WithdrawalsWallet(data,sessions["id"]))

@router.patch("/wallet",dependencies=[Depends(verify_token)])
async def read_users(is_disabled: Annotated[str, Form()]):
    return mapping_success(controller_init.DisabledWallet(sessions["id"]))

