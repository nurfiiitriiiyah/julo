from fastapi import FastAPI
from fastapi_versioning import VersionedFastAPI
from api.v1.endpoints import endpoint
app = FastAPI()

app.include_router(endpoint.router)

app = VersionedFastAPI(app,
    version_format='{major}',
    prefix_format='/api/v{major}',
)